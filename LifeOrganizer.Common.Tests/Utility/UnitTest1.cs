using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LifeOrganizer.Common;

namespace LifeOrganizer.Common.UnitTests
{
    [TestClass]
    public class GuardTest
    {
        [TestMethod]
        public void AgainstInvalidId_GivenNegativeNumberAsLong_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidId((long)(-1), "Test"));
        }

        [TestMethod]
        public void AgainstInvalidId_GivenZeroAsLong_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidId((long)0, "Test"));
        }
        
        [TestMethod]
        public void AgainstInvalidId_GivenPositiveNumberAsLong_DoesNotThrowArgumentException()
        {
            Guard.AgainstInvalidId((long)1, "Test");
        }

        [TestMethod]
        public void AgainstInvalidId_GivenNegativeNumberAsInt_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidId(-1, "Test"));
        }

        [TestMethod]
        public void AgainstInvalidId_GivenZeroAsInt_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidId(0, "Test"));
        }

        [TestMethod]
        public void AgainstInvalidId_GivenPositiveNumberAsInt_DoesNotThrowArgumentException()
        {
            Guard.AgainstInvalidId(1, "Test");
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenNegativeNumberAsDecimal_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstNegativeValue((decimal)(-1), "Test"));
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenZeroAsDecimal_DoesNotThrowArgumentException()
        {
            Guard.AgainstNegativeValue((decimal)0, "Test");
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenPositiveNumberAsDecimal_DoesNotThrowArgumentException()
        {
            Guard.AgainstNegativeValue((decimal)1, "Test");
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenNegativeNumberAsInt_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstNegativeValue((int)(-1), "Test"));
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenZeroAsInt_DoesNotThrowArgumentException()
        {
            Guard.AgainstNegativeValue((int)0, "Test");
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenPositiveNumberAsInt_DoesNotThrowArgumentException()
        {
            Guard.AgainstNegativeValue((int)1, "Test");
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenNegativeNumberAsShort_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstNegativeValue((short)(-1), "Test"));
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenZeroAsShort_DoesNotThrowArgumentException()
        {
            Guard.AgainstNegativeValue((short)0, "Test");
        }

        [TestMethod]
        public void AgainstNegativeValue_GivenPositiveNumberAsShort_DoesNotThrowArgumentException()
        {
            Guard.AgainstNegativeValue((short)1, "Test");
        }

        [TestMethod]
        public void AgainstInvalidXmlString_GivenInvalidXml_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidXmlString("Test", "Test"));
        }

        [TestMethod]
        public void AgainstInvalidXmlString_GivenEmptyXml_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidXmlString(string.Empty, "Test"));
        }

        [TestMethod]
        public void AgainstInvalidXmlString_GivenNullXml_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidXmlString(null, "Test"));
        }

        [TestMethod]
        public void AgainstInvalidXmlString_GivenWhitespaceXml_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstInvalidXmlString(" ", "Test"));
        }

        [TestMethod]
        public void AgainstInvalidXmlString_GivenValidXml_DoesNotThrowArgumentException()
        {
            Guard.AgainstInvalidXmlString("<test>Valid!</test>", "Test");
        }

        [TestMethod]
        public void AgainstNullEmptyOrWhitespaceString_GivenEmptyString_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstNullEmptyOrWhitespaceString(string.Empty, "Test"));
        }

        [TestMethod]
        public void AgainstNullEmptyOrWhitespaceString_GivenNullString_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstNullEmptyOrWhitespaceString(null, "Test"));
        }

        [TestMethod]
        public void AgainstNullEmptyOrWhitespaceString_GivenWhitespaceString_ThrowsArgumentException()
        {
            Assert.ThrowsException<ArgumentException>(() => Guard.AgainstNullEmptyOrWhitespaceString(" ", "Test"));
        }

        [TestMethod]
        public void AgainstNullEmptyOrWhitespaceString_GivenStringWithContents_DoesNotThrowArgumentException()
        {
            Guard.AgainstNullEmptyOrWhitespaceString("Test", "Test");
        }

        [TestMethod]
        public void AgainstNullObject_GivenNull_ThrowsArgumentNullException()
        {
            object test = null;

            Assert.ThrowsException<ArgumentNullException>(() => Guard.AgainstNullObject(test, nameof(test)));
        }

        [TestMethod]
        public void AgainstNullObject_GivenAnyNonNullObject_DoesNotThrowArgumentNullException()
        {
            object test = new { };

            Guard.AgainstNullObject(test, nameof(test));
        }
    }
}
